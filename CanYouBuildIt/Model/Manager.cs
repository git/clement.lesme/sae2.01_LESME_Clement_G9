﻿using CanYouBuildIt.DataContractPersistance;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CanYouBuildIt.Model
{
    public class Manager
    {
        public List<Utilisateur> listUtil { get; private set; }
        public List<Composant> listComp { get; private set; }
        public List<Build> listBuild { get; private set; }
        public IPersistanceManager Persi { get; set; }

        public Manager()
        {
            listUtil = new List<Utilisateur>();
            listComp = new List<Composant>();
            listBuild = new List<Build>();
        }
        public Manager(IPersistanceManager pers) 
        {
            listUtil = new List<Utilisateur>();
            listComp = new List<Composant>();
            listBuild = new List<Build>();
            Persi = pers;
        }

        //Utilise le chargeDonnee donnée de la persistance (de stub ou de DataContractPersi)
        public void chargeDonne()
        {
            DataToPersist donnee = Persi.chargeDonnee();
            listUtil.AddRange(donnee.lu);
            listComp.AddRange(donnee.lc);
            listBuild.AddRange(donnee.lb);
        }

        //Utilise le sauvegardeDonnee donnée de la persistance (de DataContractPersi)
        public void sauvegardeDonnee()
        {
            DataToPersist data = new DataToPersist();
            data.lu.AddRange(listUtil);
            data.lc.AddRange(listComp);
            data.lb.AddRange(listBuild);
            Persi.sauvegardeDonnee(data);
        }

        //Ajoute l'utilisateur passé en paramètre a la liste d'utilisateur du manager
        public int ajouterUtilisateur(Utilisateur utilisateur)
        {
            listUtil.Add(utilisateur);
            return listUtil.Count-1 ;
        }

        //Ajoute le composant passé en paramètre a la liste de composant du manager
        public void ajouterComposant(Composant composant)
        {
            listComp.Add(composant);
        }

        //Ajoute le build passé en paramètre a la liste de build du manager
        public void ajouterBuild(Build build)
        {
            listBuild.Add(build);
        }

        //Permet de savoir si un username existe ou si il est déjà attribué (et return l'indice dans la liste du manager)
        public int rechercheUsername(string usern)
        {
            for (int i = 0; i < listUtil.Count; i++ )
            {
                if (listUtil[i].username.Equals(usern,StringComparison.OrdinalIgnoreCase)){
                    return i;
                }
            }
            return -1;
        }

        //Permet de chercher un mot de passe pour savoir si il correspond au bonne utilisateur (et return l'indice dans la liste du manager)
        public int recherchePwd(string pwd)
        {
            for (int i = 0; i < listUtil.Count; i++)
            {
                if (listUtil[i].password.Equals(pwd,StringComparison.OrdinalIgnoreCase))
                {
                    return i;
                }
            }
            return -1;
        }

        //Recherche si un pc fais parti des builds mis en favoris par un utilisateur
        public int rechercheFav(int usern,int fav)
        {
            if (listUtil[usern].listFav != null)
                for (int i = 0; i < listUtil[usern].listFav.Count();++i)
                {
                    if (listUtil[usern].listFav[i] == fav)
                    {
                        return (i);
                    }
                }
            return -1;
        }
    }
}
