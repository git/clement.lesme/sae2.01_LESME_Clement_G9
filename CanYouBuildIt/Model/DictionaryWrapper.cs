﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CanYouBuildIt.Model
{
    public class DictionaryWrapper<TypeComposant, Composant>
    {
        [DataMember]
        public List<KeyValuePair<TypeComposant, Composant>> Items { get; set; }

        public DictionaryWrapper(Dictionary<TypeComposant, Composant> dictionary)
        {
            Items = dictionary.ToList();
        }

        public Dictionary<TypeComposant, Composant> ToDico()
        {
            return Items.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        }
    }
}
