﻿using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace CanYouBuildIt.Model
{
    [DataContract]
    public class Build
    {
        [DataMember]
        public static int nbId = 0;
        [DataMember]
        public int id { get; private set; }
        [DataMember]
        public float prix { get; private set; } = new float();
        [DataMember]
        public string image { get; private set; }
        [DataMember]
        public Dictionary<TypeComposant, Composant> dComp { get; private set;}   //Contient tous les composant d'un build, chacun étant d'un type distinct


        //Constructeur de la classe Build qui demande le nom d'une image et les composants du build
        public Build(string image,Composant v1, Composant v2, Composant v3, Composant v4, Composant v5, Composant v6, Composant v7, Composant v8 = null, Composant v9 = null, Composant v10 = null)
        {
            id = nbId;
            nbId = nbId +1;
            this.image = image;
            dComp = new Dictionary<TypeComposant, Composant>();
            prix = 0;

            //Ajoue de chaque élément + calcule du prix total
            dComp.Add(v1.type,v1);
            prix += v1.prix;
            dComp.Add(v2.type,v2);
            prix = prix + v2.prix;
            dComp.Add(v3.type,v3);
            prix = prix + v3.prix;
            dComp.Add(v4.type,v4);
            prix = prix + v4.prix;
            dComp.Add(v5.type,v5);
            prix = prix + v5.prix;
            dComp.Add(v6.type,v6);
            prix = prix + v6.prix;
            dComp.Add(v7.type,v7);
            prix = prix + v7.prix;

            prix = (float)Math.Round(prix, 2);

            //verifie si les valeurs "non obligatoire" ne sois pas null, et les rentre dans le dictionnaire
            if (v8 != null)
            {
                dComp.Add(v8.type, v8);
                prix = prix + v8.prix;

                if (v9 != null)
                {
                    dComp.Add(v9.type,v9);
                    prix = prix + v9.prix;

                    if (v10 != null)
                    {
                        dComp.Add(v10.type, v10);
                        prix = prix + v10.prix;
                    }
                }
            }
        }


    }


}
