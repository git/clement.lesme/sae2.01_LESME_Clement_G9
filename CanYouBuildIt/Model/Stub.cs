﻿using CanYouBuildIt.DataContractPersistance;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanYouBuildIt.Model
{
    public class Stub : IPersistanceManager
    {
        public Stub() { }
        public DataToPersist chargeDonnee()
        {
            List<Utilisateur> lu = new List<Utilisateur>();
            List<Composant> lc = new List<Composant>();
            
            //-- test de chargement de User --//
            Utilisateur u1 = new Utilisateur("Clément", "1234");
            Utilisateur u2 = new Utilisateur("Vivien", "vivien");
            Utilisateur u3 = new Utilisateur("Kevin", "0000");
            lu.Add(u1);
            lu.Add(u2);
            lu.Add(u3);
            

            //-- Test pour 1 build entier --//
            lc.Add(new Composant("Zalman Z10 Plus", TypeComposant.Boitier,(float)119.95, "https://www.ldlc.com/fiche/PB00502897.html"));
            lc.Add(new Composant("Ryzen 7 5800X",TypeComposant.Processeur,(float)218.98, "https://www.amazon.fr/AMD-Ryzen-7-5800X-RyzenTM/dp/B0815XFSGK/"));
            lc.Add(new Composant("Cooler Master MasterLiquid ML240L V2 ARGB Black Edition ",TypeComposant.Ventirad,(float)89.95, "https://www.ldlc.com/fiche/PB00454454.html"));
            lc.Add(new Composant("Gigabyte B550M AORUS ELITE ",TypeComposant.CarteMere,(float)129.95, "https://www.ldlc.com/fiche/PB00357556.html"));
            lc.Add(new Composant("be quiet! Pure Power 11 600W 80PLUS Gold", TypeComposant.Alimentation,(float)93.95, "https://www.ldlc.com/fiche/PB00261005.html"));
            lc.Add(new Composant("ASUS RTX 3060", TypeComposant.CarteGraphique, (float)453.99, "https://www.amazon.fr/ASUS-DUAL-NVIDIA-GeForce-GDDR6/dp/B096658ZWP"));
            lc.Add(new Composant("Kingston Kit mémoire FURY Beast 16 Go (2 x 8 Go) 3200 MHz DDR4 CL16", TypeComposant.RAM, (float)49.99, "https://www.amazon.fr/Kingston-3200MHz-M%C3%A9moire-KF432C16BBK2-16/dp/B097K2WBL3/"));
            lc.Add(new Composant("Crucial BX500 1 To",TypeComposant.SSD,(float)74.95, "https://www.ldlc.com/fiche/PB00281733.html"));
            lc.Add(new Composant("Seagate BarraCuda 2 To (ST2000DM005) ", TypeComposant.HDD,(float)62.95, "https://www.ldlc.com/fiche/PB00254339.html"));
            //-- Test pour un deuxième build --//9
            lc.Add(new Composant("Corsair Vengeance RGB RS 16 Go (2 x 8 Go) DDR4 3200 MHz CL16", TypeComposant.RAM, (float)60.70, "https://www.cdiscount.com/informatique/memoire-ram/memoire-ram-corsair-vengeance-rgb-rs-ddr4/f-10716-cor0840006649007.html?idOffre"));
            lc.Add(new Composant("Samsung SSD 980 M.2 PCIe NVMe 1 To", TypeComposant.SSD, (float)57.37, "https://www.amazon.fr/s?k=samsung+ssd+980+m.2+pcie+nvme+1to&ref=nb_sb_noss"));
            lc.Add(new Composant("NZXT H5 Flow Noir", TypeComposant.Boitier, (float)109.36, "https://www.ldlc.com/fiche/PB00532181.html"));
            lc.Add(new Composant("MARSGAMING MF-Duo", TypeComposant.Ventilateur, (float)10.90, "https://www.amazon.fr/MARSGAMING-Ventilateurs-Rainbow-Ultra-Silencieux-Connexion/dp/B0BGY9M3ZG/"));
            //-- Test pour un troisième build --//13
            lc.Add(new Composant("Corsair Vengeance LPX 16GB (2x8GB) DDR4 3200MHz C16 - Noir", TypeComposant.RAM, (float)48.89, "https://www.amazon.fr/Corsair-Vengeance-16GB-2x8GB-3200MHz/dp/B07RS1G6XW/ref=asc_df_B07RS1G6XW/?tag=go"));
            lc.Add(new Composant("Disque SSD Interne Kingston FURY Renegade 1000 Go Noir", TypeComposant.SSD, (float)99.99, "https://www.fnac.com/Disque-SSD-Interne-Kingston-FURY-Renegade-1000-Go-Noir/a17744537/w-4?Origin=SEA_GOOGLE_PLA_MICRO&esl-k=sem-google%7cnx%7cc%7cm%7ckpla%7cp%7ct%7cdc%7ca%7cg19917047259&gclid=CjwKCAjwvpCkBhB4EiwAujULMrICm_eRaleVBUXER177e-Bw14n1mBVECGMwnR1fDSr5hJF5w5V1GRoCnIMQAvD_BwE&gclsrc=aw.ds"));
            //8
            lc.Add(new Composant("Corsair CV Series CV550Watt, 80 Plus Bronze", TypeComposant.Alimentation, (float)64.99, "https://www.amazon.fr/Alimentation-Corsair-CV550Watt-Plus-Bronze/dp/B07YVWW7MW/ref=asc_df_B07YVWW7MW/?tag=googshopfr-21&linkCode=df0&hvadid=411330562965&hvpos=&hvnetw=g&hvrand=991396465070491192&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=9055886&hvtargid=pla-896516658390&psc=1&tag=&ref=&adgrpid=94631885692&hvpone=&hvptwo=&hvadid=411330562965&hvpos=&hvnetw=g&hvrand=991396465070491192&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=9055886&hvtargid=pla-896516658390"));
            //5
            lc.Add(new Composant("Intel® Core™ i5-13400F", TypeComposant.Processeur, (float)214.65, "https://www.rueducommerce.fr/p-intel-core-i5-13400f-25ghz46ghz-intel-3436638-18383.html?gclid=CjwKCAjwvpCkBhB4EiwAujULMpR3j0-QYXXpmJ-8xhkSlYtW14MqWqznls6gKGuzZlqd3xovMzKOORoCdLsQAvD_BwE"));
            lc.Add(new Composant("ASUS TUF GAMING B760-PLUS WIFI D4", TypeComposant.CarteMere, (float)219.90, "https://infomaxparis.com/fr/carte-mere-pc-gamer/32759-carte-mere-gamer-asus-tuf-gaming-b760-plus-wifi-d4-4711387009499.html?gclid=CjwKCAjwvpCkBhB4EiwAujULMjBU334W9KjPkQBMo9KIFlDEidI-7XWt9RZZ6cAzXeYFOj5IGoHojhoCUp0QAvD_BwE"));
            lc.Add(new Composant("be quiet! Pure Rock 2 Black", TypeComposant.Ventirad, (float)44.95, "https://www.ldlc.com/fiche/PB00340585.html?utm_source=google&utm_medium=cpc&utm_campaign=Google+Ads&gclid=CjwKCAjwvpCkBhB4EiwAujULMkokWrjANEI5KX2M0RDxb1ahQCIdgPRw-hO3Xhr7HltyvzzGtdcHIhoCf8wQAvD_BwE"));
            lc.Add(new Composant("Boitier PC Corsair 4000X RGB - Blanc", TypeComposant.Boitier,(float)125.90,"https://infomaxparis.com/fr/boitier-pc-gamer/31342-corsair-4000x-rgb-blanc-840006626657.html?gclid=CjwKCAjwvpCkBhB4EiwAujULMn3Y0EsT_TRsUKqczvsf5X3J3T052ep45BDBF7_qmUTDIFaLXNHY6xoCQDYQAvD_BwE"));

            DataToPersist data = new DataToPersist();

            data.lu.AddRange(lu);
            data.lc.AddRange(lc);

            Build b1 = new Build("pc1.png", lc[0], lc[1], lc[2], lc[3], lc[4], lc[5], lc[6], lc[7], lc[8]);
            data.lb.Add(b1);
            data.lb.Add(new Build("pc2.png", lc[11], lc[1], lc[2], lc[3], lc[4], lc[5], lc[9], lc[8], lc[10], lc[12]));
            data.lb.Add(new Build("pc3.png", lc[13], lc[14], lc[15], lc[16], lc[17], lc[18], lc[8], lc[5], lc[19]));


            return data;
        }

        public void sauvegardeDonnee(DataToPersist data)
        { }
    }
}
