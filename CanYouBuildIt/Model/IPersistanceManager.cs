﻿using CanYouBuildIt.DataContractPersistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanYouBuildIt.Model
{
    //Permet de donnée un type commun au stub et au DataContractPersi pour pouvoir passer de un à l'autre 
    //sans changer de type d'attribut dans le manager
    public interface IPersistanceManager
    {
        public DataToPersist chargeDonnee();

        public void sauvegardeDonnee(DataToPersist data);
    }
}
