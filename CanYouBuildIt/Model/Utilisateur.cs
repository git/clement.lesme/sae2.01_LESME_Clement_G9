﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CanYouBuildIt.Model
{
    [DataContract]
    public class Utilisateur
    {
        [DataMember]
        public string username { get; private set; }
        [DataMember]
        public string password { get; private set; }
        [DataMember]
        public List<Build> listBuild = new List<Build>();
        [DataMember]
        public List<int> listFav = new List<int>();
        [DataMember]
        public List<int> Builds = new List<int>();


        public Utilisateur(string name, string pass, List<Build> listbuild = null, List<int> listfav = null, List<int> builds = null)
        {
            username = name;
            password = pass;
            if (listbuild != null)
                listBuild = listbuild;
            else
                listBuild = new List<Build>();
            if (listfav != null)
                listFav = listfav;
            else
                listFav = new List<int>();
            if (listbuild != null)
                Builds = builds;
            else
                Builds = new List<int>();
        }
    }
}
