﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CanYouBuildIt.Model
{
    //Permet de définir strictement les types qu'un Composant peut avoir
    public enum TypeComposant
    {
        Boitier,
        Processeur,
        Ventirad,
        CarteMere,
        RAM,
        CarteGraphique,
        Alimentation,
        Ventilateur,
        SSD,
        HDD
    }

    [DataContract]
    public class Composant
    {
        [DataMember]
        public string id { get; private set; }
        [DataMember]
        public TypeComposant type { get; private set; }
        [DataMember]
        public float prix { get; private set; }
        [DataMember]
        public string lien { get; private set; }


        public Composant( string idComp, TypeComposant typeComp, float prixCompo, string lienComp)
        {
            id = idComp;
            type = typeComp;
            prix = prixCompo;
            lien = lienComp;
        }
    }
}
