﻿using CanYouBuildIt.Model;
using Microsoft.Maui.Controls;
using System.Diagnostics;

namespace CanYouBuildIt;

public partial class App : Application
{
	// -- Défini si on charge les données depuis un fichier ou le stub -- //
	public Manager manager { get; private set; } = new Manager(new Stub());
	//public Manager manager { get; private set; } = new Manager(new DataContractPersistance.DataContractPers());
	public App()
	{
        manager.chargeDonne();
		manager.Persi = new DataContractPersistance.DataContractPers();
		manager.sauvegardeDonnee();
		Debug.WriteLine(manager.listUtil[0]);
		InitializeComponent();
        MainPage = new AppShell();
	}
}