using CanYouBuildIt.Model;
using System.Diagnostics;

namespace CanYouBuildIt.Views;

public partial class Ordinateur : ContentPage
{
    public Manager manager => (App.Current as App).manager;
    public Dictionary<TypeComposant, Composant> dico = new Dictionary<TypeComposant, Composant>();
    int user { get; set;}
    int idPc { get; set;}

    public Ordinateur(int IdPc)
	{
		InitializeComponent();
        dico = manager.listBuild[IdPc].dComp;
        fillGrid(IdPc);
	}

    public Ordinateur(int IdPc,int idUser)
    {
        InitializeComponent();
        dico = manager.listBuild[IdPc].dComp;
        idPc = IdPc;
        fillGrid(IdPc);
        bouttonFav(idUser);
    }

    //d�pile la fen�tre pour retourner � la pr�c�dente
    public async void BackHome(object sender, EventArgs e)
    {

        await Navigation.PopAsync();
    }

    public void bouttonFav(int user)
    {
        //if (manager.rechercheFav(user, idPc) != -1)
        if (manager.listUtil[user].listFav.Contains(idPc))
        {
            ImageButton favbtn = new ImageButton()
            {
                Source = "favon.png",
                HeightRequest = 50,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Start
            };
            favbtn.Clicked += switchFav;
            Grid.SetRow(favbtn, 1);
            Grid.SetColumn(favbtn, 1);
            pcG.Children.Add(favbtn);
        }
        else
        {
            ImageButton favbtn = new ImageButton()
            {
                Source = "favoff.png",
                HeightRequest = 50,
                HorizontalOptions = LayoutOptions.End,
                VerticalOptions = LayoutOptions.Start
            };
            favbtn.Clicked += switchFav;
            Grid.SetRow(favbtn, 1);
            Grid.SetColumn(favbtn, 1);
            pcG.Children.Add(favbtn);
        }
        
    }

    public void switchFav(object sender, EventArgs e)
    {
        ImageButton imageButton = (ImageButton)sender;

        if (imageButton.Source.ToString() == "favon.png")
        {
            imageButton.Source = "favoff.png";
            int adressFav = manager.rechercheFav(user, idPc);
            manager.listUtil[user].listFav.RemoveAt(adressFav);
            manager.sauvegardeDonnee();
        }
        else
        {
            imageButton.Source = "favon.png";
            manager.listUtil[user].listFav.Add(idPc);
            manager.sauvegardeDonnee();
        }
    }

    public void fillGrid(int Pc)
	{

        // -- Processeur -- //
        Label cpu = new Label()
        {
            Text = dico[TypeComposant.Processeur].id,
            TextColor = Color.FromArgb("#FFFFFF"),
            HorizontalOptions = LayoutOptions.Center, 
            VerticalOptions = LayoutOptions.Center
        };
        //D�fini la case de la grille ou sera l'image
        Grid.SetRow(cpu, 0);
        Grid.SetColumn(cpu, 0);
        //Met l'image dans la grille
        pcG.Children.Add(cpu);

        // -- Boitier -- //
        Label boite = new Label()
        {
            Text = dico[TypeComposant.Boitier].id,
            TextColor = Color.FromArgb("#FFFFFF"),
            HorizontalOptions = LayoutOptions.Center,
            VerticalOptions = LayoutOptions.Center
        };
        Grid.SetRow(boite, 0);
        Grid.SetColumn(boite, 1);
        pcG.Children.Add(boite);

        // -- RAM -- //
        Label ram = new Label()
        {
            Text = dico[TypeComposant.RAM].id,
            TextColor = Color.FromArgb("#FFFFFF"),
            HorizontalOptions = LayoutOptions.Center,
            VerticalOptions = LayoutOptions.Center
        };
        Grid.SetRow(ram, 0);
        Grid.SetColumn(ram, 2);
        pcG.Children.Add(ram);

        // -- Carte Graphique -- //
        Label gpu = new Label()
        {
            Text = dico[TypeComposant.CarteGraphique].id,
            TextColor = Color.FromArgb("#FFFFFF"),
            HorizontalOptions = LayoutOptions.Center,
            VerticalOptions = LayoutOptions.Center
        };
        Grid.SetRow(gpu, 1);
        Grid.SetColumn(gpu, 0);
        pcG.Children.Add(gpu);

        // -- Ventirad -- //
        Label ventir = new Label()
        {
            Text = dico[TypeComposant.Ventirad].id,
            TextColor = Color.FromArgb("#FFFFFF"),
            HorizontalOptions = LayoutOptions.Center,
            VerticalOptions = LayoutOptions.Center
        };
        Grid.SetRow(ventir, 1);
        Grid.SetColumn(ventir, 2);
        pcG.Children.Add(ventir);

        // -- Carte M�re -- //
        Label mboard = new Label()
        {
            Text = dico[TypeComposant.CarteMere].id,
            TextColor = Color.FromArgb("#FFFFFF"),
            HorizontalOptions = LayoutOptions.Center,
            VerticalOptions = LayoutOptions.Center
        };
        Grid.SetRow(mboard, 2);
        Grid.SetColumn(mboard, 0);
        pcG.Children.Add(mboard);

        // -- Ventilateur -- //
        if (dico.ContainsKey(TypeComposant.Ventilateur))
        {
            Label ventilo = new Label()
            {
                Text = dico[TypeComposant.Ventilateur].id,
                TextColor = Color.FromArgb("#FFFFFF"),
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center
            };
            Grid.SetRow(ventilo, 2);
            Grid.SetColumn(ventilo, 2);
            pcG.Children.Add(ventilo);
        }
        else
        {
            Label ventilo = new Label()
            {
                Text = "Pas de ventilateur supl�mentaire",
                TextColor = Color.FromArgb("#FFFFFF"),
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center
            };
            Grid.SetRow(ventilo, 2);
            Grid.SetColumn(ventilo, 2);
            pcG.Children.Add(ventilo);
        }

        // -- SSD -- //
        if (dico.ContainsKey(TypeComposant.SSD))
        {
            Label ssd = new Label()
            {
                Text = dico[TypeComposant.SSD].id,
                TextColor = Color.FromArgb("#FFFFFF"),
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center
            };
            Grid.SetRow(ssd, 3);
            Grid.SetColumn(ssd, 0);
            pcG.Children.Add(ssd);
        }
        else
        {
            Label ssd = new Label()
            {
                Text = "Pas de SSD",
                TextColor = Color.FromArgb("#FFFFFF"),
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center
            };
            Grid.SetRow(ssd, 3);
            Grid.SetColumn(ssd, 0);
            pcG.Children.Add(ssd);
        }


        // -- Alimentation -- //
        Label alim = new Label()
        {
            Text = dico[TypeComposant.Alimentation].id,
            TextColor = Color.FromArgb("#FFFFFF"),
            HorizontalOptions = LayoutOptions.Center,
            VerticalOptions = LayoutOptions.Center
        };
        Grid.SetRow(alim, 3);
        Grid.SetColumn(alim, 1);
        pcG.Children.Add(alim);

        // -- HDD -- //
        if (dico.ContainsKey(TypeComposant.HDD)) 
        { 
            Label hdd = new Label()
            {
                Text = dico[TypeComposant.HDD].id,
                TextColor = Color.FromArgb("#FFFFFF"),
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center
            };
            Grid.SetRow(hdd, 3);
            Grid.SetColumn(hdd, 2);
            pcG.Children.Add(hdd);
        }
        else
        {
            Label hdd = new Label()
            {
                Text = "Pas de HDD",
                TextColor = Color.FromArgb("#FFFFFF"),
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center
            };
            Grid.SetRow(hdd, 3);
            Grid.SetColumn(hdd, 2);
            pcG.Children.Add(hdd);
        }

        Image pcI = new Image()
        {
            Source = manager.listBuild[Pc].image,
            Margin = new Thickness(0,-50,0,0),
            HeightRequest = 300,
        };
        Grid.SetRow(pcI, 2);
        Grid.SetColumn(pcI, 1);
        pcG.Children.Add(pcI);

        prixL.Text = manager.listBuild[idPc].prix.ToString();
    }


}