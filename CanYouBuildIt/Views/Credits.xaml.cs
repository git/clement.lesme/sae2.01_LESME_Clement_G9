namespace CanYouBuildIt.Views;

public partial class Credits : ContentPage
{
    public Credits()
    {
        InitializeComponent();
    }

    //d�pile la fen�tre pour retourner � la pr�c�dente
    public async void BackHome(object sender, EventArgs e)
    {

        await Navigation.PopAsync();
    }

    // -- Les 3 fonctions suivantes ouvre dans un navigateur les sites internet suivant : -- //
    //LDLC
    public async void lienLDLC(object sender, EventArgs e)
    {
        await Launcher.OpenAsync(new Uri("https://www.ldlc.com/configurateur/"));
    }
    //CanYouRunIt
    public async void lienCanYou(object sender, EventArgs e)
    {
        await Launcher.OpenAsync(new Uri("https://www.systemrequirementslab.com/cyri"));
    }
    //LeD�nicheur
    public async void lienDenicheur(object sender, EventArgs e)
    {
        await Launcher.OpenAsync(new Uri("https://ledenicheur.fr/"));
    }
}