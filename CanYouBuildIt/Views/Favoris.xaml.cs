using CanYouBuildIt.Model;
using System.Diagnostics;

namespace CanYouBuildIt.Views;

public partial class Favoris : ContentPage
{
    public Manager manager => (App.Current as App).manager;
    int user;

	public Favoris(int iduser)
	{
		InitializeComponent();
		user = iduser;
	}

    //d�pile la fen�tre pour retourner � la pr�c�dente
    public async void BackHome(object sender, EventArgs e)
	{
        await Navigation.PopAsync();	
	}

    //Empile la fenetre d'un pc
    public async void NavOrdi(object sender, EventArgs e)
    {
        // R�cup�rer l'ImageButton qui a d�clench� l'�v�nement
        ImageButton imageButton = (ImageButton)sender;

        // R�cup�rer l'objet Build associ� � l'image cliqu�e
        Build build = (Build)imageButton.BindingContext;

        // Acc�der � la propri�t� id de l'objet Build
        int id = build.id;
        await Navigation.PushAsync(new Ordinateur(id, user));
    }

    public void showGrid()
    {
        int row = 0;
        for (int i = 0; i < 7 && i < manager.listUtil[user].listFav.Count(); ++i)
        {
            if (i % 2 == 0)
                row = 0;
            else
                row = 1;

            int util = manager.listUtil[user].listFav[i];

            ImageButton imagebtn = new ImageButton()
            {
                Source = manager.listBuild[util].image,
                BackgroundColor = Color.FromHex("#119FA8"),
                BindingContext = manager.listBuild[util],
                CornerRadius = 10,
                HeightRequest = 200,
                Margin = new Thickness(1.5, 0, 1.5, 0)
            };
            imagebtn.Clicked += NavOrdi;

            //D�fini la case de la grille ou sera l'image
            Grid.SetRow(imagebtn, row);
            Grid.SetColumn(imagebtn, i);
            //Met l'image dans la grille
            gridacc.Children.Add(imagebtn);

        }
    }
}