using CanYouBuildIt.Model;
using System.Diagnostics;

namespace CanYouBuildIt.Views;

public partial class Acceuil : ContentPage
{
    public Manager manager => (App.Current as App).manager;     //Permet d'acceder � toutes les valeurs sauvegarder de l'app
    bool connecte = false;                                      //Permet de v�rifier si un utilisateur est connecter (pour par exemple acceder � ses favoris
    int user;                                                   //Adresse de l'utilisateur connecter dans la liste d'utilisateur du manager

    //Constructeur par default utiliser lors de l'ouverture de l'app
    public Acceuil()
    {
        InitializeComponent();
        BindingContext = manager;
        showGrid();
    }

    //Constructeur utiliser lors de la connexion d'un utilisateur.
    public Acceuil(int iduser)
    {
        InitializeComponent();
        BindingContext = manager;
        connecte = true;
        user = iduser;
        string userN = manager.listUtil[user].username;
        Log.Text = userN;
        showGrid();

    }

    //Ancien test
    /*public void AddUtilisater(object sender, EventArgs e)
	{
		Utilisateur utilisateur = new Utilisateur("Util1","pdm1");
		Debug.WriteLine("Utilisateur cr��");
	}*/

    //V�rifie si l'utilisateur et connecter puis empile la fenetre des favoris de l'utilisateur
    public async void NavFav(object sender, EventArgs e)
    {
        if (connecte)
            await Navigation.PushAsync(new Favoris(user));
        else
        {
            aConnecter.IsVisible = true;
            await Task.Delay(3000);
            aConnecter.IsVisible = false;
        }
    }

    //Empile la fenetre des credits
    public async void NavCreds(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new Credits());
    }

    //Empile la fenetre de connexion
    public async void NavLogin(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new Login());
    }

    //Empile la fenetre d'un pc
    public async void NavOrdi(object sender, EventArgs e)
    {
        // R�cup�rer l'ImageButton qui a d�clench� l'�v�nement
        ImageButton imageButton = (ImageButton)sender;

        // R�cup�rer l'objet Build associ� � l'image cliqu�e
        Build build = (Build)imageButton.BindingContext;

        // Acc�der � la propri�t� id de l'objet Build
        int id = build.id;
        if (connecte)
            await Navigation.PushAsync(new Ordinateur(id,user));
        else
            await Navigation.PushAsync(new Ordinateur(id));
    }

    //Ferme l'application
    public void ToQuit(object sender, EventArgs e)
    {
        App.Current.Quit();
    }

    //Permet d'afficher les pc selon leur nombre dans la grid de ma page d'accueil
    public void showGrid()
    {
        int row = 0;
        for (int i = 0; i < 7 && i < manager.listBuild.Count(); ++i)
        {
            if (i % 2 == 0)
                row = 0;
            else
                row = 1;

            ImageButton imagebtn = new ImageButton()
            {
                Source = manager.listBuild[i].image,
                BackgroundColor = Color.FromHex("#119FA8"),
                BindingContext = manager.listBuild[i],
                CornerRadius = 10,
                HeightRequest = 200,
                Margin = new Thickness(1.5,0,1.5,0)
            };
            imagebtn.Clicked += NavOrdi;

            //D�fini la case de la grille ou sera l'image
            Grid.SetRow(imagebtn, row);
            Grid.SetColumn(imagebtn, i);
            //Met l'image dans la grille
            gridacc.Children.Add(imagebtn);

        }
    }

}