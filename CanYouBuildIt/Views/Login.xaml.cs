namespace CanYouBuildIt.Views;
using CanYouBuildIt.Model;

public partial class Login : ContentPage
{
    public Manager manager => (App.Current as App).manager;     //Permet d'acceder � toutes les valeurs sauvegarder de l'app

    public Login()
    {
        InitializeComponent();
    }

    //Boutton de confirmation des infos entr�e dans les champs
    private async void BoutonValider(object sender, EventArgs e)
    {
        error.IsVisible = false;
        wrong.IsVisible = false;
        //recup�rer les valeurs d'entr�es
        string MdpUlti = Mdp.Text;
        string Name = Nom.Text;


        //V�rifie si tout les crit�res sont valid� :
        //Tous les champs remplis
        if (Name == null || MdpUlti == null)
            error.IsVisible = true;
        else
        {
            int idname = manager.rechercheUsername(Name);
            int idmdp = manager.recherchePwd(MdpUlti);

            //Pseudo et mot de passe existant
            if (idname == -1 && idmdp == -1)
                wrong.IsVisible = true;
            //Mot de passe correspondant � l'utilisateur
            else if(idname != idmdp)
                wrong.IsVisible = true;
            else 
            {
                valide.IsVisible = true;
                await Task.Delay(1000);
                valide.IsVisible = false;
                await Navigation.PushAsync(new Acceuil(idname));
            }
        }
    }

    //d�pile la fen�tre pour retourner � la pr�c�dente
    private async void BackHome(object sender, EventArgs e)
    {
        await Navigation.PopAsync();
    }

    //Empile la fenetre d'Inscription
    private async void NavSignIn(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new SignIn());
    }
}