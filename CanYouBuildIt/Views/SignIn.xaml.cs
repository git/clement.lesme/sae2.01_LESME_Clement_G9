namespace CanYouBuildIt.Views;
using CanYouBuildIt.Model;

public partial class SignIn : ContentPage
{
    public Manager manager => (App.Current as App).manager;
    public SignIn()
	{
		InitializeComponent();
	}

    public async void BtValider(object sender, EventArgs e)
    {
        vide.IsVisible = false;
        wrong.IsVisible = false;
        nequal.IsVisible = false;
        userNameUsed.IsVisible = false;

        //recup�rer les valeurs d'entr�es
        string MdpUtil = Mdp.Text;
        string cMdpUtil = cMdp.Text;
        string Name = Nom.Text;

        //V�rifie si tout les crit�res sont valid� :
        //Tous les champs remplis
        if (Name == null || MdpUtil == null || cMdpUtil ==null)
            vide.IsVisible = true;
        //Aucun autre user avec le m�me pseudo
        else if (manager.rechercheUsername(Name) != -1)
            userNameUsed.IsVisible = true;
        //Mot de passe de plus de 4 caract�res
        else if (MdpUtil.Length <4)
            wrong.IsVisible=true;
        //Mot de passe et confirmation �gale
        else if (MdpUtil != cMdpUtil)
            nequal.IsVisible = true;
        else 
        {
            valide.IsVisible = true;
            await Task.Delay(1000);
            valide.IsVisible = false;
            int id = manager.ajouterUtilisateur(new Utilisateur(Name,MdpUtil));
            manager.sauvegardeDonnee();
            await Navigation.PushAsync(new Acceuil(id));
        }
    }

    //d�pile la fen�tre pour retourner � la pr�c�dente
    public async void BackLogin(object sender, EventArgs e)
    {
        await Navigation.PopAsync();
    }
}