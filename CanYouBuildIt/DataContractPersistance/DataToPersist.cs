﻿using CanYouBuildIt.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanYouBuildIt.DataContractPersistance
{
    //Crée une structure de donnée pour pouvoir renvoyer plusieurs type à la fois
    public class DataToPersist
    {
        public List<Utilisateur> lu = new List<Utilisateur>();
        public List<Composant> lc = new List<Composant>();
        public List<Build> lb = new List<Build>();
        public List<Dictionary<TypeComposant,Composant>> ld = new List<Dictionary<TypeComposant, Composant>>();
    }
}
