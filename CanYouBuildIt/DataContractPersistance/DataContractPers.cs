﻿using CanYouBuildIt.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CanYouBuildIt.DataContractPersistance
{
    public class DataContractPers : IPersistanceManager
    {
        public string FilePath { get; set; }
        public string FileName { get; set; } = "UserData.xml";
        public string FileDico { get; set; } = "DicoData.xml";

        //Charge les données sauvegardées dans un fichier de format xml
        public DataToPersist chargeDonnee()
        {
            //-- Chemin de sauvegarde --//
            string appDirectory = AppContext.BaseDirectory;
            int sousDossier = 53;
            FilePath = appDirectory.Substring(0, appDirectory.Length - sousDossier);
            Directory.SetCurrentDirectory(FilePath);
            FilePath = Path.Combine(FilePath, "Data");


            var serializer = new DataContractSerializer(typeof(DataToPersist));
            DataToPersist data = new DataToPersist();
            //-- Read fichier--//
            using (Stream stream = File.OpenRead(Path.Combine(FilePath, FileName)))
            {
                data = serializer.ReadObject(stream) as DataToPersist;
            }
            //data = chargeDico(data);
            return data;
        }
    
        /*public DataToPersist chargeDico(DataToPersist data)
        {
            var serializer = new DataContractSerializer(typeof(Dictionary<TypeComposant, Composant>));
            //-- Read fichier--//
            using (Stream stream = File.OpenRead(Path.Combine(FilePath, FileName)))
            {
                data.ld = serializer.ReadObject(stream) as Dictionary<TypeComposant, Composant>;
            }

            return data;
        }*/

        //Sauvegarde les données dans un fichier de format xml
        public void sauvegardeDonnee(DataToPersist data)
        {
            //-- Chemin de sauvegarde --//
            string appDirectory = AppContext.BaseDirectory;
            int sousDossier = 53;
            FilePath = appDirectory.Substring(0, appDirectory.Length - sousDossier);
            Directory.SetCurrentDirectory(FilePath);
            FilePath = Path.Combine(FilePath, "Data");

            //-- Vérifie si le fichier et dossier existe déjà --//
            
            if (!Directory.Exists(FilePath))
            {
                Directory.CreateDirectory(FilePath);
            }

            var serializer = new DataContractSerializer (typeof(DataToPersist));
            //-- Write fichier --//
            var settings = new XmlWriterSettings() { Indent = true };
            using (TextWriter tw = File.CreateText(Path.Combine(FilePath, FileName)))
            {
                using (XmlWriter writer = XmlWriter.Create(tw, settings))
                {
                    serializer.WriteObject(writer, data);
                }
            }
            //sauvegardeDico(data.ld);
        }

        /*
        //Sauvegarde les données des dictionaires de composants dans un fichier de format xml
        public void sauvegardeDico(List<Dictionary<TypeComposant, Composant>> ld) 
        {
            var serializer = new DataContractSerializer(typeof(Dictionary<TypeComposant, Composant>));
            //-- Write fichier --//
            var settings = new XmlWriterSettings() { Indent = true };
            using (TextWriter tw = File.CreateText(Path.Combine(FilePath, FileDico)))
            {
                using (XmlWriter writer = XmlWriter.Create(tw, settings))
                {
                    serializer.WriteObject(writer, ld);
                }
            }
        }*/

    }
}
